import { parseFromDOM as tocParseFromDOM } from './toc'

document.addEventListener('DOMContentLoaded', () => {
  const tableOfContents = tocParseFromDOM()
  tableOfContents.findAndActivate()
  document.addEventListener('scroll', () => {
    tableOfContents.findAndActivate()
  })
})