export class FlatItem {
  constructor (
    readonly domID: string,
    readonly contentElement: HTMLElement,
    readonly linkElement: HTMLElement,
  ) {}
}

const activeClasses = ['bg-amber-400']

export class Item {
  constructor (
    readonly domID: string,
    readonly contentElement: HTMLElement,
    readonly linkElement: HTMLElement,
    readonly list: List | null,
  ) {}

  get flat (): FlatItem[] {
    const flatItems: FlatItem[] = [new FlatItem(
      this.domID,
      this.contentElement,
      this.linkElement
    )]
    if (this.list !== null) {
      flatItems.push(...this.list.flat)
    }
    return flatItems
  }

  isAboveThreshold (): boolean {
    return this.contentElement.getBoundingClientRect().top < window.innerHeight * 0.5
  }

  activate () {
    this.linkElement.classList.add(...activeClasses)
    this.list?.findAndActivate()
  }

  deactivateAll () {
    this.linkElement.classList.remove(...activeClasses)
    this.list?.deactivateAll()
  }
}

export class List {
  constructor (
    readonly items: Item[]
  ) {}

  get flat (): FlatItem[] {
    const flatItems: FlatItem[] = []
    this.items.forEach((item: Item) => {
      flatItems.push(...item.flat)
    })
    return flatItems
  }

  findAndActivate () {
    for (let i = this.items.length - 1; i >= 0; i--) {
      if (this.items[i].isAboveThreshold()) {
        this.items[i].activate()
        break
      }
    }
  }

  deactivateAll () {
    this.items.forEach((item: Item) => item.deactivateAll())
  }
}

export class TableOfContents {
  constructor (
    readonly list: List | null
  ) {}

  get flat (): FlatItem[] {
    if (this.list === null) {
      return []
    }
    return this.list.flat
  }

  findAndActivate () {
    this.list?.deactivateAll()
    this.list?.findAndActivate()
  }
}

const parseItems = (parentElement: HTMLElement): Item[] => {
  const itemRawElements = parentElement.querySelectorAll(':scope > li')
  if (itemRawElements.length === 0) {
    throw new Error(`empty list ${parentElement}`)
  }
  const items: Item[] = []
  itemRawElements.forEach((itemRawElement: Element) => {
    const itemElement = itemRawElement as HTMLElement
    const domID = itemElement.dataset.id as string
    const contentElement = document.getElementById(domID)
    if (contentElement === null) {
      throw new Error(`element ${domID} was not found`)
    }
    const linkRawElements = itemElement.querySelectorAll(':scope > a')
    if (linkRawElements.length === 0) {
      throw new Error(`no link found for ${itemElement}`)
    }
    if (linkRawElements.length > 1) {
      throw new Error(`more than 1 link found (${linkRawElements.length}) for ${itemElement}`)
    }
    const linkElement = linkRawElements.item(0) as HTMLElement
    items.push(new Item(domID, contentElement, linkElement, parseList(itemElement)))
  })
  return items
}

const parseList = (parentElement: HTMLElement): List | null => {
  const listRawElements = parentElement.querySelectorAll(':scope > ol')
  switch (listRawElements.length) {
    case 0:
      return null
    case 1:
      const listElement = listRawElements.item(0) as HTMLElement
      return new List(parseItems(listElement))
    default:
      throw new Error(`more than 1 list found (${listRawElements.length}) for ${parentElement}`)
  }
}

export const parseFromDOM = (): TableOfContents => {
  const tocDataElement = document.getElementById('table-of-contents')
  if (tocDataElement == null) {
    throw new Error('toc DOM element not found')
  }
  return new TableOfContents(parseList(tocDataElement))
}