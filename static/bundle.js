/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nvar toc_1 = __webpack_require__(/*! ./toc */ \"./src/toc.ts\");\n// const tocDataElement = document.getElementById('toc-data')\n// if (tocDataElement === null) {\n//   throw new Error('could not find TOC data element')\n// }\n// const toc = JSON.parse(tocDataElement.innerText)\n//\n// const el = document.getElementById(toc[1].id)\n// if (el === null) {\n//   throw new Error('zzz')\n// }\n// const rect = el.getBoundingClientRect()\n// console.log(rect)\n// el.classList.add('bg-indigo-800')\n//\n// document.addEventListener('scroll', (event: Event) => {\n//   console.log(window.innerHeight)\n//   console.log(el.getBoundingClientRect().y)\n//\n// })\ndocument.addEventListener('DOMContentLoaded', function () {\n    var tableOfContents = (0, toc_1.parseFromDOM)();\n    tableOfContents.findAndActivate();\n    document.addEventListener('scroll', function () {\n        tableOfContents.findAndActivate();\n    });\n});\n\n\n//# sourceURL=webpack://zola-theme/./src/index.ts?");

/***/ }),

/***/ "./src/toc.ts":
/*!********************!*\
  !*** ./src/toc.ts ***!
  \********************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nexports.parseFromDOM = exports.TableOfContents = exports.List = exports.Item = exports.FlatItem = void 0;\nvar FlatItem = /** @class */ (function () {\n    function FlatItem(domID, contentElement, linkElement) {\n        this.domID = domID;\n        this.contentElement = contentElement;\n        this.linkElement = linkElement;\n    }\n    return FlatItem;\n}());\nexports.FlatItem = FlatItem;\nvar activeClasses = ['bg-amber-400'];\nvar Item = /** @class */ (function () {\n    function Item(domID, contentElement, linkElement, list) {\n        this.domID = domID;\n        this.contentElement = contentElement;\n        this.linkElement = linkElement;\n        this.list = list;\n    }\n    Object.defineProperty(Item.prototype, \"flat\", {\n        get: function () {\n            var flatItems = [new FlatItem(this.domID, this.contentElement, this.linkElement)];\n            if (this.list !== null) {\n                flatItems.push.apply(flatItems, this.list.flat);\n            }\n            return flatItems;\n        },\n        enumerable: false,\n        configurable: true\n    });\n    Item.prototype.isAboveThreshold = function () {\n        return this.contentElement.getBoundingClientRect().top < window.innerHeight * 0.5;\n    };\n    Item.prototype.activate = function () {\n        var _a;\n        var _b;\n        (_a = this.linkElement.classList).add.apply(_a, activeClasses);\n        (_b = this.list) === null || _b === void 0 ? void 0 : _b.findAndActivate();\n    };\n    Item.prototype.deactivateAll = function () {\n        var _a;\n        var _b;\n        (_a = this.linkElement.classList).remove.apply(_a, activeClasses);\n        (_b = this.list) === null || _b === void 0 ? void 0 : _b.deactivateAll();\n    };\n    return Item;\n}());\nexports.Item = Item;\nvar List = /** @class */ (function () {\n    function List(items) {\n        this.items = items;\n    }\n    Object.defineProperty(List.prototype, \"flat\", {\n        get: function () {\n            var flatItems = [];\n            this.items.forEach(function (item) {\n                flatItems.push.apply(flatItems, item.flat);\n            });\n            return flatItems;\n        },\n        enumerable: false,\n        configurable: true\n    });\n    List.prototype.findAndActivate = function () {\n        for (var i = this.items.length - 1; i >= 0; i--) {\n            if (this.items[i].isAboveThreshold()) {\n                this.items[i].activate();\n                break;\n            }\n        }\n    };\n    List.prototype.deactivateAll = function () {\n        this.items.forEach(function (item) { return item.deactivateAll(); });\n    };\n    return List;\n}());\nexports.List = List;\nvar TableOfContents = /** @class */ (function () {\n    function TableOfContents(list) {\n        this.list = list;\n    }\n    Object.defineProperty(TableOfContents.prototype, \"flat\", {\n        get: function () {\n            if (this.list === null) {\n                return [];\n            }\n            return this.list.flat;\n        },\n        enumerable: false,\n        configurable: true\n    });\n    TableOfContents.prototype.findAndActivate = function () {\n        var _a, _b;\n        (_a = this.list) === null || _a === void 0 ? void 0 : _a.deactivateAll();\n        (_b = this.list) === null || _b === void 0 ? void 0 : _b.findAndActivate();\n    };\n    return TableOfContents;\n}());\nexports.TableOfContents = TableOfContents;\nvar parseItems = function (parentElement) {\n    var itemRawElements = parentElement.querySelectorAll(':scope > li');\n    if (itemRawElements.length === 0) {\n        throw new Error(\"empty list \".concat(parentElement));\n    }\n    var items = [];\n    itemRawElements.forEach(function (itemRawElement) {\n        var itemElement = itemRawElement;\n        var domID = itemElement.dataset.id;\n        var contentElement = document.getElementById(domID);\n        if (contentElement === null) {\n            throw new Error(\"element \".concat(domID, \" was not found\"));\n        }\n        var linkRawElements = itemElement.querySelectorAll(':scope > a');\n        if (linkRawElements.length === 0) {\n            throw new Error(\"no link found for \".concat(itemElement));\n        }\n        if (linkRawElements.length > 1) {\n            throw new Error(\"more than 1 link found (\".concat(linkRawElements.length, \") for \").concat(itemElement));\n        }\n        var linkElement = linkRawElements.item(0);\n        items.push(new Item(domID, contentElement, linkElement, parseList(itemElement)));\n    });\n    return items;\n};\nvar parseList = function (parentElement) {\n    var listRawElements = parentElement.querySelectorAll(':scope > ol');\n    switch (listRawElements.length) {\n        case 0:\n            return null;\n        case 1:\n            var listElement = listRawElements.item(0);\n            return new List(parseItems(listElement));\n        default:\n            throw new Error(\"more than 1 list found (\".concat(listRawElements.length, \") for \").concat(parentElement));\n    }\n};\nvar parseFromDOM = function () {\n    var tocDataElement = document.getElementById('table-of-contents');\n    if (tocDataElement == null) {\n        throw new Error('toc DOM element not found');\n    }\n    return new TableOfContents(parseList(tocDataElement));\n};\nexports.parseFromDOM = parseFromDOM;\n\n\n//# sourceURL=webpack://zola-theme/./src/toc.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.ts");
/******/ 	
/******/ })()
;